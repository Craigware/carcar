from django.contrib import admin
from .models import AutoMobileVO, SalesPerson, Customer, Sale


@admin.register(AutoMobileVO)
class AutoMobileVOAdmin(admin.ModelAdmin):
    pass


@admin.register(SalesPerson)
class SalesPersonAdmin(admin.ModelAdmin):
    pass


@admin.register(Customer)
class CustomerAdmin(admin.ModelAdmin):
    pass


@admin.register(Sale)
class SaleAdmin(admin.ModelAdmin):
    pass