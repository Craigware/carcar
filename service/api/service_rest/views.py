from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from .json_responses import *
from .models import AutomobileVO, Technician, Appointment


@require_http_methods(["GET", "POST"])
def api_list_technicians(request):
    if request.method == "GET":
        technician_list = Technician.objects.all();
        return json_encoded_technician_list(technician_list)

    elif request.method == "POST":
        content = json.loads(request.body)
        try:
            technician = Technician.objects.create(**content)
            return json_encoded_technician(technician)
        except:
            return json_invalid()


@require_http_methods(["GET", "PUT", "DELETE"])
def api_detail_technicians(request, pk):
    if request.method == "GET":
        try:
            technician = Technician.objects.get(employee_id=pk)
            return json_encoded_technician(technician)
        except:
            return json_technician_id_not_found(pk)

    if request.method == "PUT":
        try:
            content = json.loads(request.body)
            technician = Technician.objects.filter(employee_id=pk)
            try:
                technician.update(**content)
                return json_encoded_technician(technician)
            except:
                return json_invalid()
        except Technician.DoesNotExist:
            return json_technician_id_not_found(pk)

    else:
        try:
            technician = Technician.objects.get(employee_id=pk)
            technician.delete()
            return json_technician_deleted(pk)
        except Technician.DoesNotExist:
            return json_technician_id_not_found(pk)


@require_http_methods(["GET", "POST"])
def api_list_appointments(request):
    if request.method == "GET":
        appointments_list = Appointment.objects.all()
        return json_encoded_appointment_list(appointments_list)
    else:
        json_content = json.loads(request.body)

        try:
            auto = AutomobileVO.objects.get(vin=json_content["vin"])
            technician = Technician.objects.get(employee_id=json_content["employee_id"])
        except Technician.DoesNotExist:
                return json_technician_id_not_found(json_content["employee_id"])
        except AutomobileVO.DoesNotExist:
                return json_auto_vin_not_found(json_content["vin"])

        try:
            loaded_content = {
                "date_time": json_content["date_time"],
                "reason": json_content["reason"],
                "status": "SCHEDULED",
                "customer": json_content["customer"],
                "technician": technician,
                "auto": auto
            }
            appointment = Appointment.objects.create(**loaded_content)

            return json_encoded_appointment(appointment)
        except:
            return json_invalid()



@require_http_methods(["DELETE","PUT","GET"])
def api_detail_appointments(request, pk):
    if request.method == "GET":
        try:
            appointment = Appointment.objects.get(id=pk)
            return json_encoded_appointment(appointment);
        except Appointment.DoesNotExist:
            return json_appointment_not_found(pk);

    if request.method == "PUT":
        if Appointment.objects.filter(id=pk):
            content = json.loads(request.body)
            appointment = Appointment.objects.filter(id=pk)
            try:
                technician = Technician.objects.get(employee_id=content["employee_id"])
                automobile = AutomobileVO.objects.get(vin=content["vin"])

                try:
                    loaded_content = {
                        "date_time": content["date_time"],
                        "reason": content["reason"],
                        "status": content["status"],
                        "customer": content["customer"],
                        "technician": technician,
                        "auto": automobile
                    }

                    appointment.update(**loaded_content)
                    return json_encoded_appointment(appointment)

                except:
                    return json_invalid();

            except Technician.DoesNotExist:
                return json_technician_id_not_found(content["employee_id"])
            except AutomobileVO.DoesNotExist:
                return json_auto_vin_not_found(content["vin"])

        else:
            return json_appointment_not_found(pk)

    else:
        try:
            appointment = Appointment.objects.get(id=pk)
            appointment.delete()
            return json_appointment_deleted(pk)
        except Appointment.DoesNotExist:
            return json_appointment_not_found(pk)


def api_cancel_appointment(request, pk):
    if Appointment.objects.filter(id=pk):
        appointment = Appointment.objects.filter(id=pk)
        appointment.update(status="CANCELED")
        return json_encoded_appointment(appointment)
    else:
        return json_appointment_not_found(pk)


def api_finish_appointment(request, pk):
    if Appointment.objects.filter(id=pk):
        appointment = Appointment.objects.filter(id=pk)
        appointment.update(status="FINISHED")
        return json_encoded_appointment(appointment)
    else:
        return json_appointment_not_found(pk)
