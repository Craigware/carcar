import React, {useState, useEffect} from "react";


function AutomobileForm(){
    const [formData, setFormData] = useState({
        color: '',
        year: '',
        vin: '',
        model_id: '',
    });
    const [models, setModels] = useState([]);


    const handleSubmit = async (e) => {
        e.preventDefault();

        const url = 'http://localhost:8100/api/automobiles/';

        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(formData),
            headers: {
                'Content-type': 'application/json',
            },
        };

        const response = await fetch(url, fetchConfig);

        if(response.ok){
            setFormData({
                color: '',
                year: '',
                vin: '',
                model_id: '',
            });
        }
    }


    const handleFormChange = (e) => {
        const value = e.target.value;

        const inputName = e.target.name;

        setFormData({
            ...formData,
            [inputName]: value,
        });
    }


    const fetchModel = async () => {
        const response = await fetch('http://localhost:8100/api/models/');

        if (response.ok){
            const data = await response.json();
            setModels(data.models);
        }
        else{
            console.warn(response);
        }
    }


    useEffect(() => {
        fetchModel();
    }, []);


    return(
        <div className="row">
            <div className="offset-2 col-8">
                <div className="shadow-lg p-4 mt-4">
                    <h1>Add automobile</h1>
                    <form onSubmit={handleSubmit} id="create_automobile">
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} value={formData.color} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                            <label htmlFor="color" className="text-muted">Color</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} value={formData.year} placeholder="Year" required type="text" name="year" id="year" className="form-control" />
                            <label htmlFor="year" className="text-muted">Year</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} value={formData.vin} placeholder="Vin" required type="text" name="vin" id="vin" className="form-control" />
                            <label htmlFor="vin" className="text-muted">VIN</label>
                        </div>
                        <div className="form-floating mb-3">
                            <select onChange={handleFormChange} value={formData.model_id} placeholder="Model" name="model_id" id="model_id" className="form-control">
                                <option value="">Select a model</option>
                                {models.map(model => {
                                    return (<option value={model.id} key={model.id}>{model.name}</option>)
                                })}
                            </select>
                            <label htmlFor="model" className="text-muted">Vehicle Model</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}


export default AutomobileForm;
