import { useState, useEffect } from "react";


function SalesList(){
    const [sales, setSales] = useState([]);

    const getData = async () => {
        const response = await fetch('http://localhost:8090/api/sales/');
        if (response.ok){
            const data = await response.json();
            setSales(data.sale);
        }
    }

    const deleteData = async (event) => {
        event.preventDefault();
        const id = event.target.value;
        const salesUrl = `http://localhost:8090/api/sales/${id}/`;
        const fetchConfig = {
            method: "delete",
        };

        await fetch(salesUrl, fetchConfig);
        getData();
    }


    useEffect(() => {
        getData();
    }, []);


    return(
        <table className="table">
            <thead>
                <tr>
                    <th>First Name</th>
                    <th>Employee ID</th>
                    <th>Customer</th>
                    <th>Automobile VIN</th>
                    <th>Price</th>
                    <th>Delete</th>
                </tr>
            </thead>

            <tbody>
                {sales.map((sale) => {
                    return (
                        <tr key={sale.id}>
                            <td>{sale.sales_person.first_name}</td>
                            <td>{sale.sales_person.employee_id}</td>
                            <td>{sale.customer.first_name}</td>
                            <td>{sale.automobile.vin}</td>
                            <td>${sale.price}</td>
                            <td>
                                <button onClick={deleteData} value={sale.id} className="btn btn-danger">Delete</button>
                            </td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
    );

}


export default SalesList;
