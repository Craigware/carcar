import React, {useState, useEffect} from "react";


function SalesForm() {
    const [automobile, setAutomobile] = useState([]);
    const [salesPerson, setSalesPerson] = useState([]);
    const [customer, setCustomer] = useState([]);
    const [formData, setFormData] = useState({
        automobile: '',
        sales_person: '',
        customer: '',
        price: '',
    });
    const reload = () => {
        return "hey";
    }


    const getAutomobile = async () => {
        const response = await fetch('http://localhost:8090/api/autocheck/');

        if (response.ok){
            const data = await response.json();
            setAutomobile(data.notsold);
        }
    }


    const getSalesPerson = async () => {
        const response = await fetch('http://localhost:8090/api/salespeople/');

        if (response.ok){
            const data = await response.json();
            setSalesPerson(data.sales_person);
        }
    }


    const getCustomer = async () => {
        const response = await fetch('http://localhost:8090/api/customers/');

        if (response.ok){
            const data = await response.json();
            setCustomer(data.customer);
        }
    }


    const handleSubmit = async(e) => {
        e.preventDefault();
        const url = 'http://localhost:8090/api/sales/';
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(formData),
            headers: {
                'Content-type': 'application/json',
            },
        };
        const response = await fetch(url, fetchConfig);
        if(response.ok){
            setFormData({
                automobile: '',
                sales_person: '',
                customer: '',
                price: '',
            });
            reload();
        }
    }


    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({
            ...formData,
            [inputName]: value,
        });
    }


    useEffect(() => {
        getAutomobile();
        getSalesPerson();
        getCustomer();
        reload();
    }, []);


    return(
        <div className="row">
            <div className="offset-2 col-8">
                <div className="shadow-lg p-4 mt-4">
                    <h1>Create Sale</h1>
                    <form onSubmit={handleSubmit} id="create_salesperson">
                        <div className="form-floating mb-3">
                            <select onChange={handleFormChange} value={formData.automobile} placeholder="Automobile" name="automobile" id="automobile" className="form-control">
                            <option value="">Select a automobile</option>
                                {automobile.map(automobiles => {
                                    return (<option value={automobiles.href} key={automobiles.href}>{automobiles.vin}</option>)
                                })}
                            </select>
                            <label htmlFor="price" className="text-muted">Automobile</label>
                        </div>
                        <div className="form-floating mb-3">
                            <select onChange={handleFormChange} value={formData.sales_person} placeholder="Salesperson" name="sales_person" id="sales_person" className="form-control">
                                <option value="">Select a sales person</option>
                                {salesPerson.map(salespersons => {
                                    return (<option value={salespersons.id} key={salespersons.id}>{salespersons.first_name} {salespersons.last_name}</option>)
                                })}
                            </select>
                            <label htmlFor="sales_person" className="text-muted">Sales Person</label>
                        </div>
                        <div className="form-floating mb-3">
                            <select onChange={handleFormChange} value={formData.customer} placeholder="Customer" name="customer" id="customer" className="form-control">
                                <option value="">Select a customer</option>
                                {customer.map(customers => {
                                    return (<option value={customers.id} key={customers.id}>{customers.first_name} {customers.last_name}</option>)
                                })}
                            </select>
                            <label htmlFor="customer" className="text-muted">Customer</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} value={formData.price} placeholder="Price" required type="text" name="price" id="price" className="form-control" />
                            <label htmlFor="price" className="text-muted">Price</label>
                        </div>
                    <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}


export default SalesForm;
