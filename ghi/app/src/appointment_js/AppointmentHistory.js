import React, { useEffect, useState } from "react";


function AppointmentHistory(){
    const [appointments, setAppointments] = useState([]);
    const [sales, setSales] = useState([]);
    const [vin, setVin] = useState("");
    const [search, setSearch] = useState("");


    const handleVin = (event) => {
        const value = event.target.value;
        setVin(value);
    }


    const handleSearch = (event) => {
        event.preventDefault();
        setSearch(vin);
    }


    const fetchSales = async () => {
        const salesUrl = 'http://localhost:8090/api/sales/';
        const salesResponse = await fetch(salesUrl);
        if (salesResponse.ok) {
            const salesData = await salesResponse.json();
            setSales(salesData.sale);
        }
    }


    const fetchAppointments = async () => {
        const appointmentUrl = 'http://localhost:8080/api/appointments/';
        const appointmentResponse = await fetch(appointmentUrl);
        if (appointmentResponse.ok) {
            const appointmentData = await appointmentResponse.json();
            setAppointments(appointmentData.appointments);
        }
    }


    useEffect(() => {
        fetchAppointments();
        fetchSales();
    }, []);


    return (
        <div>
            <h1 className="mt-3">Service History</h1>
            <div className="form-floating mt-3">
                <form>
                    <input onChange={handleVin} value={vin} placeholder="Insert VIN" required type="text" id="vin" name="vin" />
                    <button onClick={handleSearch} className="btn btn-primary">Search</button>
                </form>
            </div>
            <div>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>VIN</th>
                            <th>Customer</th>
                            <th>VIP Status</th>
                            <th>Date</th>
                            <th>Time</th>
                            <th>Technician</th>
                            <th>Reason</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        {appointments.map(appointment => {
                            const dateTime = new Date(appointment.date_time);
                            const time = dateTime.toLocaleTimeString([], {hour: '2-digit', minute: "2-digit"});
                            const date = dateTime.toLocaleDateString();
                            const vipStatus = () => {
                                const salesContainsVip = sales.filter(sale => sale.automobile.vin === appointment.vin);
                                if (salesContainsVip.length > 0){
                                    return "TRUE";
                                }
                                return "FALSE";
                            }

                            if (appointment.vin === search){
                                return (
                                    <tr key={appointment.id}>
                                        <td>{appointment.vin}</td>
                                        <td>{appointment.customer}</td>
                                        <td>{vipStatus()}</td>
                                        <td>{date}</td>
                                        <td>{time}</td>
                                        <td>{appointment.technician.first_name}</td>
                                        <td>{appointment.reason}</td>
                                        <td>{appointment.status}</td>
                                    </tr>
                                );
                            } else if (search === ""){
                                return (
                                    <tr key={appointment.id}>
                                        <td>{appointment.vin}</td>
                                        <td>{appointment.customer}</td>
                                        <td>{vipStatus()}</td>
                                        <td>{date}</td>
                                        <td>{time}</td>
                                        <td>{appointment.technician.first_name}</td>
                                        <td>{appointment.reason}</td>
                                        <td>{appointment.status}</td>
                                    </tr>
                                );
                            }
                        })}
                    </tbody>
                </table>
            </div>
        </div>
    );
}


export default AppointmentHistory;
