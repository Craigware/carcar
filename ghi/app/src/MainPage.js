import { Carousel } from "react-bootstrap";
import { useEffect, useState } from "react";
import blueCircle from './images/Blue-Circle.png'
import clientServerConnection from './images/Client-Server-Connection.png'
import browserIcon from './images/Browser-Icon.png'


function MainPage() {
    const [models, setModels] = useState([]);


    const fetchModels = async () => {
        const response = await fetch('http://localhost:8100/api/models/');

        if (response.ok){
            const data = await response.json();
            setModels(data.models);
        }
        else{
            console.warn(response);
        }
    }


    useEffect(() => {
        fetchModels();
    }, [])


    return (
        <div>
            <div id="carousel" className="wrapper">
                <h1 className="absolute-pos-carousel-title text-light bg-transparent-black px-3 py-2">Models In Inventory</h1>
                <Carousel className="height-500">

                    {models.map((model, index) => (
                    <Carousel.Item key={index} className="">
                        <img className="d-block w-100 fit-parrent" src={model.picture_url} />
                        <Carousel.Caption>{model.name}</Carousel.Caption>
                    </Carousel.Item>
                    ))}
                </Carousel>
            </div>


            <div className="px-4 py-5 text-center" id="title">
                <h1 className="display-5 fw-bold text-dark">CarCar</h1>
                <div className="col-lg-6 mx-auto">
                    <p className="lead mb-2 text-dark">
                        The premiere solution for automobile dealership
                        management!
                    </p>
                </div>
            </div>

            <div className="container text-center mt-5 mb-2">
                <div className="row align-items-center">
                    <div className="col">
                        <div className="wrapper">
                            <div className="absolute-pos-benefits">
                                <p className="fs-5 card-pos">Through the utilization of REST APIs you can populate your database from
                                a sleak and easy to use front-end!</p>
                            </div>

                            <div className="absolute-pos-benefits">
                                <img src={clientServerConnection} style={{position: "absolute", top: "-20rem", left: "-3.5rem", zIndex: "1"}} />
                            </div>
                        </div>
                        <img src={blueCircle} />
                    </div>

                    <div className="col">
                        <div className="wrapper">
                            <div className="text-center" style={{position: "absolute", top: "22.5rem", left: "6rem"}}>
                                <p className="fs-5 card-pos">Visualize your back-end through our powerful lists!</p>
                            </div>

                            <div className="absolute-pos-benefits">
                                <img src={browserIcon} style={{position: "absolute", top: "-18rem", left: "9rem", zIndex: "1"}} />
                            </div>
                        </div>
                        <img src={blueCircle} />
                    </div>
                </div>
            </div>

            <div className="seperator-m" />
        </div>
    );
}


export default MainPage;
