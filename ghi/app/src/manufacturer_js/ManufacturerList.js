import React, {useEffect, useState} from "react";


function ManufacturerList(){
    const [manufacturers, setManufacturers] = useState([]);


    const fetchManufacturers = async () => {
        const manufacturerUrl = 'http://localhost:8100/api/manufacturers/';
        const manufacturerResponse = await fetch(manufacturerUrl);
        if (manufacturerResponse.ok) {
            const manufacturerData = await manufacturerResponse.json();
            setManufacturers(manufacturerData.manufacturers);
        }
    }

    const deleteData = async (event) => {
        event.preventDefault();
        const id = event.target.value;
        const manufacturerUrl = `http://localhost:8100/api/manufacturers/${id}/`;
        const fetchConfig = {
            method: "delete",
        };

        await fetch(manufacturerUrl, fetchConfig);
        fetchManufacturers();
    }


    useEffect(() => {
        fetchManufacturers();
    }, []);


    return (
        <div>
            <h1 className="display-6 mt-2"><strong>Manufacturers</strong></h1>
            <table className="table table-striped mt-2 table-hover">
                <thead className="heading-success">
                    <tr>
                        <th>Manufacturer</th>
                        <th>Delete</th>
                    </tr>
                </thead>
                <tbody className="">
                    {manufacturers.map(manufacturer => {
                        return (
                            <tr key={manufacturer.name}>
                                <td>{manufacturer.name}</td>
                                <td>
                                    <button onClick={deleteData} value={manufacturer.id} className="btn btn-danger">Delete</button>
                                </td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    );
}


export default ManufacturerList;
