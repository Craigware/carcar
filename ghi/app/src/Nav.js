import { NavLink } from 'react-router-dom';


function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
        <div className="container-fluid">
            <NavLink className="navbar-brand" to="/">CarCar</NavLink>
            <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarSupportedContent">
                <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                    <li className="nav-item dropdown">
                        <a className="nav-link dropdown-toggle" hrzf="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                            Automobiles
                        </a>
                        <ul className="dropdown-menu">
                            <NavLink className="nav-link dropdown-item text-black" aria-current="page" to="/automobiles/create/">Create Automobile</NavLink>
                            <NavLink className="nav-link dropdown-item text-black" aria-current="page" to="/automobiles/">Automobile List</NavLink>
                        </ul>
                    </li>

                    <li className="nav-item dropdown">
                        <a className="nav-link dropdown-toggle" hrzf="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                            Manufacturer
                        </a>
                        <ul className="dropdown-menu">
                            <NavLink className="nav-link dropdown-item text-black" aria-current="page" to="/manufacturers/create">Create Manufacturer</NavLink>
                            <NavLink className="nav-link dropdown-item text-black" aria-current="page" to="/manufacturers/">Manufacturer List</NavLink>
                        </ul>
                    </li>

                    <li className="nav-item dropdown">
                        <a className="nav-link dropdown-toggle" hrzf="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                            Vehicle Models
                        </a>
                        <ul className="dropdown-menu">
                            <NavLink className="nav-link dropdown-item text-black" aria-current="page" to="/models/create">Create Model</NavLink>
                            <NavLink className="nav-link dropdown-item text-black" aria-current="page" to="/models/">Model List</NavLink>
                        </ul>
                    </li>

                    <li className="nav-item dropdown">
                        <a className="nav-link dropdown-toggle" hrzf="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                            Sales People
                        </a>
                        <ul className="dropdown-menu">
                            <NavLink className="nav-link dropdown-item text-black" aria-current="page" to="/salespeople/create/">Create Sales Person</NavLink>
                            <NavLink className="nav-link dropdown-item text-black" aria-current="page" to="/salespeople/">Sales People List</NavLink>
                            <NavLink className="nav-link dropdown-item text-black" aria-current="page" to="/salespeople/select/">Sales People Select</NavLink>
                        </ul>
                    </li>

                    <li className="nav-item dropdown">
                        <a className="nav-link dropdown-toggle" hrzf="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                            Customers
                        </a>
                        <ul className="dropdown-menu">
                            <NavLink className="nav-link dropdown-item text-black" aria-current="page" to="/customers/create/">Create Customer</NavLink>
                            <NavLink className="nav-link dropdown-item text-black" aria-current="page" to="/customers/">Customer List</NavLink>
                        </ul>
                    </li>

                    <li className="nav-item dropdown">
                        <a className="nav-link dropdown-toggle" hrzf="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                            Sales
                        </a>
                        <ul className="dropdown-menu">
                            <NavLink className="nav-link dropdown-item text-black" aria-current="page" to="/sales/create/">Create Sale</NavLink>
                            <NavLink className="nav-link dropdown-item text-black" aria-current="page" to="/sales/">Sales List</NavLink>
                        </ul>
                    </li>

                    <li className="nav-item dropdown">
                        <a className="nav-link dropdown-toggle" hrzf="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                            Technicians
                        </a>
                        <ul className="dropdown-menu">
                            <NavLink className="nav-link dropdown-item text-black" aria-current="page" to="/technicians/create/">Create Technician</NavLink>
                            <NavLink className="nav-link dropdown-item text-black" aria-current="page" to="/technicians/">Technician List</NavLink>
                        </ul>
                    </li>

                    <li className="nav-item dropdown">
                        <a className="nav-link dropdown-toggle" hrzf="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                            Appointments
                        </a>
                        <ul className="dropdown-menu">
                            <NavLink className="nav-link dropdown-item text-black" aria-current="page" to="/appointments/create/">Create Appointment</NavLink>
                            <NavLink className="nav-link dropdown-item text-black" aria-current="page" to="/appointments/">Appointment List</NavLink>
                            <NavLink className="nav-link dropdown-item text-black" aria-current="page" to="/appointments/history/">Appointment History</NavLink>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
  )
}


export default Nav;
