import React, {useState, useEffect} from 'react';


function SalesPersonSelect() {
    const [salesPerson, setSalesPerson] = useState([]);
    const [sales, setSales] = useState([]);
    const [select, setSelect] = useState("");


    const handleSelect = async (e) => {
        const value = e.target.value;
        setSelect(value);
    };


    const getSalesPerson = async () => {
        const response = await fetch('http://localhost:8090/api/salespeople/');

        if (response.ok) {
            const data = await response.json();
            setSalesPerson(data.sales_person);
        }
    }


    const getSales = async () => {
        const response = await fetch('http://localhost:8090/api/sales/');

        if (response.ok) {
            const data = await response.json();
            setSales(data.sale);
        }
    }


    useEffect(() => {
        getSalesPerson();
        getSales();
        setSelect();
    }, []);


    return(
        <div className='row'>
            <div className='mb-3'>
                <select onChange={handleSelect} value={select} required name='sales_person' id='sales_person' className='form-select'>
                    <option value=''>Choose sales person</option>
                    {salesPerson.map(salespeople => {
                        return(
                        <option key={salespeople.employee_id} value={salespeople.employee_id}>
                            {salespeople.first_name}
                        </option>);
                    })}
                </select>
            </div>
            <table className='table'>
                <thead>
                    <tr>
                        <th>Sales Person First Name</th>
                        <th>Sales Person Last Name</th>
                        <th>Customer First Name</th>
                        <th>Customer Last Name</th>
                        <th>Vin</th>
                        <th>Price</th>
                    </tr>
                </thead>
                <tbody>
                    {sales.map(salespeople => {
                        if (select !== "" && salespeople.sales_person.employee_id === select){
                            return (
                                <tr key={salespeople.id}>
                                    <td>{salespeople.sales_person.first_name}</td>
                                    <td>{salespeople.sales_person.last_name}</td>
                                    <td>{salespeople.customer.first_name}</td>
                                    <td>{salespeople.customer.last_name}</td>
                                    <td>{salespeople.automobile.vin}</td>
                                    <td>{salespeople.price}</td>
                                </tr>
                            )
                        }
                    })}
                </tbody>
            </table>
        </div>
    )
}


export default SalesPersonSelect;
