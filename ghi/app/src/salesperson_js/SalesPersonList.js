import { useState, useEffect } from "react";


function SalesPersonList(){
    const [salesperson, setSalesperson] = useState([]);

    const getData = async () => {
        const response = await fetch('http://localhost:8090/api/salespeople/');
        if (response.ok){
            const data = await response.json();
            setSalesperson(data.sales_person);
        }
    }


    const deleteData = async (event) => {
        event.preventDefault();
        const id = event.target.value;
        const salesPeopleUrl = `http://localhost:8090/api/salespeople/${id}/`;
        const fetchConfig = {
            method: "delete",
        };

        await fetch(salesPeopleUrl, fetchConfig);
        getData();
    }


    useEffect(() => {
        getData();
    }, []);


    return(
        <table className="table">
            <thead>
                <tr>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Employee ID</th>
                    <th>Delete</th>
                </tr>
            </thead>
            <tbody>
                {salesperson.map((salespersons) => {
                    return (
                        <tr key={salespersons.id}>
                            <td>{salespersons.first_name}</td>
                            <td>{salespersons.last_name}</td>
                            <td>{salespersons.employee_id}</td>
                            <td>
                                <button onClick={deleteData} value={salespersons.id} className="btn btn-danger">Delete</button>
                            </td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
    );

}


export default SalesPersonList;
